--- Compare Poker Hands! ---

This program compares two different poker hands and indicates which, if either, has a higher rank.

1. 	Run the program via console using "java -jar PokerHands.jar" or use the source code.

2. 	You can now add five cards to the first hand. Every card has a suit (C,D,H,S) and a value (2,3,4,5,6,7,8,9,T,J,Q,K,A).
	Enter the card as follows: "SUITVALUE"	-e.g.: "D8"
	Note: wrong input will result in a message "wrong input" or "card already ingame" and you will get another chance to enter a card.
	
3.	Enter five cards for the second hand.

4.	The program compares the two hands and announces the winner (or a draw).

5.	You are now asked if you want to play another round. Enter "y" to play again.
	Note: Every other input than "y" will end the program.