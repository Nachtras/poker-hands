package junitTests;

import static org.junit.Assert.*;

import org.junit.Test;

import pokerHands.Hand;
import pokerHands.LogicTable;

public class RankTests {

	@Test
	public void compare2Straights() {
		
		LogicTable table = new LogicTable();
		Hand firstHand = new Hand();
		table.addCardToHand(firstHand, "S9");
		table.addCardToHand(firstHand, "D8");
		table.addCardToHand(firstHand, "H7");
		table.addCardToHand(firstHand, "S6");
		table.addCardToHand(firstHand, "C5");
		
		Hand secondHand = new Hand();
		table.addCardToHand(secondHand, "CQ");
		table.addCardToHand(secondHand, "DJ");
		table.addCardToHand(secondHand, "HT");
		table.addCardToHand(secondHand, "S8");
		table.addCardToHand(secondHand, "C9");
		
		assertEquals("get winner ", "Second hand wins", table.getWinner(firstHand, secondHand));
	}
	
	@Test
	public void compare2StraightFlushs() {
		
		LogicTable table = new LogicTable();
		Hand firstHand = new Hand();
		table.addCardToHand(firstHand, "S9");
		table.addCardToHand(firstHand, "S8");
		table.addCardToHand(firstHand, "S7");
		table.addCardToHand(firstHand, "S6");
		table.addCardToHand(firstHand, "S5");
		
		Hand secondHand = new Hand();
		table.addCardToHand(secondHand, "DQ");
		table.addCardToHand(secondHand, "DJ");
		table.addCardToHand(secondHand, "DT");
		table.addCardToHand(secondHand, "D8");
		table.addCardToHand(secondHand, "D9");
		
		assertEquals("get winner ", "Second hand wins", table.getWinner(firstHand, secondHand));
	}

	@Test
	public void compare2Fours() {
		
		LogicTable table = new LogicTable();
		Hand firstHand = new Hand();
		table.addCardToHand(firstHand, "S9");
		table.addCardToHand(firstHand, "D9");
		table.addCardToHand(firstHand, "H9");
		table.addCardToHand(firstHand, "C9");
		table.addCardToHand(firstHand, "S5");
		
		Hand secondHand = new Hand();
		table.addCardToHand(secondHand, "S6");
		table.addCardToHand(secondHand, "D6");
		table.addCardToHand(secondHand, "H6");
		table.addCardToHand(secondHand, "C6");
		table.addCardToHand(secondHand, "HT");
		
		assertEquals("get winner ", "First hand wins", table.getWinner(firstHand, secondHand));
	}
	
	@Test
	public void compare2FullHouses() {
		
		LogicTable table = new LogicTable();
		Hand firstHand = new Hand();
		table.addCardToHand(firstHand, "S9");
		table.addCardToHand(firstHand, "D9");
		table.addCardToHand(firstHand, "H9");
		table.addCardToHand(firstHand, "D5");
		table.addCardToHand(firstHand, "S5");
		
		Hand secondHand = new Hand();
		table.addCardToHand(secondHand, "SJ");
		table.addCardToHand(secondHand, "DJ");
		table.addCardToHand(secondHand, "HJ");
		table.addCardToHand(secondHand, "CT");
		table.addCardToHand(secondHand, "HT");
		
		assertEquals("get winner ", "Second hand wins", table.getWinner(firstHand, secondHand));
	}
	
	@Test
	public void compare2Threes() {
		
		LogicTable table = new LogicTable();
		Hand firstHand = new Hand();
		table.addCardToHand(firstHand, "S9");
		table.addCardToHand(firstHand, "D9");
		table.addCardToHand(firstHand, "H9");
		table.addCardToHand(firstHand, "D4");
		table.addCardToHand(firstHand, "S2");
		
		Hand secondHand = new Hand();
		table.addCardToHand(secondHand, "S7");
		table.addCardToHand(secondHand, "D7");
		table.addCardToHand(secondHand, "H7");
		table.addCardToHand(secondHand, "CA");
		table.addCardToHand(secondHand, "HJ");
		
		assertEquals("get winner ", "First hand wins", table.getWinner(firstHand, secondHand));
	}
	
	@Test
	public void compare2Flushes() {
		
		LogicTable table = new LogicTable();
		Hand firstHand = new Hand();
		table.addCardToHand(firstHand, "SA");
		table.addCardToHand(firstHand, "ST");
		table.addCardToHand(firstHand, "S3");
		table.addCardToHand(firstHand, "SQ");
		table.addCardToHand(firstHand, "S6");
		
		Hand secondHand = new Hand();
		table.addCardToHand(secondHand, "DA");
		table.addCardToHand(secondHand, "DQ");
		table.addCardToHand(secondHand, "DT");
		table.addCardToHand(secondHand, "D6");
		table.addCardToHand(secondHand, "D4");
		
		assertEquals("get winner ", "Second hand wins", table.getWinner(firstHand, secondHand));
	}
	
	@Test
	public void compare2HighCards() {
		
		LogicTable table = new LogicTable();
		Hand firstHand = new Hand();
		table.addCardToHand(firstHand, "SK");
		table.addCardToHand(firstHand, "DJ");
		table.addCardToHand(firstHand, "H3");
		table.addCardToHand(firstHand, "C7");
		table.addCardToHand(firstHand, "S8");
		
		Hand secondHand = new Hand();
		table.addCardToHand(secondHand, "D3");
		table.addCardToHand(secondHand, "D8");
		table.addCardToHand(secondHand, "HJ");
		table.addCardToHand(secondHand, "C4");
		table.addCardToHand(secondHand, "CK");
		
		assertEquals("get winner ", "First hand wins", table.getWinner(firstHand, secondHand));
	}
	
	@Test
	public void compare2Pairs() {
		
		LogicTable table = new LogicTable();
		Hand firstHand = new Hand();
		table.addCardToHand(firstHand, "H3");
		table.addCardToHand(firstHand, "D8");
		table.addCardToHand(firstHand, "H4");
		table.addCardToHand(firstHand, "CT");
		table.addCardToHand(firstHand, "C8");
		
		Hand secondHand = new Hand();
		table.addCardToHand(secondHand, "D3");
		table.addCardToHand(secondHand, "DJ");
		table.addCardToHand(secondHand, "HQ");
		table.addCardToHand(secondHand, "CQ");
		table.addCardToHand(secondHand, "CK");
		
		assertEquals("get winner ", "Second hand wins", table.getWinner(firstHand, secondHand));
	}
	
	@Test
	public void compare2TwoPairs() {
		
		LogicTable table = new LogicTable();
		Hand firstHand = new Hand();
		table.addCardToHand(firstHand, "HQ");
		table.addCardToHand(firstHand, "D8");
		table.addCardToHand(firstHand, "H4");
		table.addCardToHand(firstHand, "CQ");
		table.addCardToHand(firstHand, "C8");
		
		Hand secondHand = new Hand();
		table.addCardToHand(secondHand, "D5");
		table.addCardToHand(secondHand, "DJ");
		table.addCardToHand(secondHand, "DQ");
		table.addCardToHand(secondHand, "SQ");
		table.addCardToHand(secondHand, "C5");
		
		assertEquals("get winner ", "First hand wins", table.getWinner(firstHand, secondHand));
	}
	
	@Test
	public void compareWheelWithStraight() {
		
		LogicTable table = new LogicTable();
		Hand firstHand = new Hand();
		table.addCardToHand(firstHand, "HA");
		table.addCardToHand(firstHand, "D5");
		table.addCardToHand(firstHand, "H2");
		table.addCardToHand(firstHand, "C3");
		table.addCardToHand(firstHand, "C4");
		
		Hand secondHand = new Hand();
		table.addCardToHand(secondHand, "DQ");
		table.addCardToHand(secondHand, "DJ");
		table.addCardToHand(secondHand, "DA");
		table.addCardToHand(secondHand, "ST");
		table.addCardToHand(secondHand, "CK");
		
		assertEquals("get winner ", "Second hand wins", table.getWinner(firstHand, secondHand));
	}
}
