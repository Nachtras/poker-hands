package junitTests;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;

import org.junit.Test;

import pokerHands.LogicTable;
import pokerHands.PokerHands;

public class PokerTests {

	@Test
	public void letUserPlayOneRound() {
		
		LogicTable table = new LogicTable();
		ByteArrayInputStream in = new ByteArrayInputStream("C7\nD7\nH7\nD4\nSQ\nCT\nHT\nST\nDT\nS2\n".getBytes());  
        System.setIn(in);  
        
		assertEquals("winner? ", "Second hand wins", table.compareHands());
		
	}
	
	//@Test //NOTE: only works if table.compareHands() in .playRounds() is disabled
	public void multipleRounds() {
		
		ByteArrayInputStream in = new ByteArrayInputStream("y\nn".getBytes());  
        System.setIn(in);  
        
		assertEquals("rounds ", 2, PokerHands.playRounds());
		
	}

}
