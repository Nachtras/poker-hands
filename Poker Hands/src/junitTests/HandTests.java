package junitTests;

import static org.junit.Assert.*;

import org.junit.Test;

import pokerHands.Card;
import pokerHands.Hand;

public class HandTests {
	
	// Rank the hand; 0 = HighCard - 8 = Straight Flush
	@Test
	public void rankMyHandFour() {
		
		Hand hand = new Hand();
		hand.addCard(new Card("C","6"));
		hand.addCard(new Card("D","A"));
		hand.addCard(new Card("H","A"));
		hand.addCard(new Card("S","A"));
		hand.addCard(new Card("C","A"));

		assertEquals("This Rank ", 7, hand.rankHand());
	}
	
	@Test
	public void rankMyHandFullHouse() {
		
		Hand hand = new Hand();
		hand.addCard(new Card("C","4"));
		hand.addCard(new Card("D","4"));
		hand.addCard(new Card("H","4"));
		hand.addCard(new Card("S","J"));
		hand.addCard(new Card("C","J"));

		assertEquals("This Rank ", 6, hand.rankHand());
	}
	
	@Test
	public void rankMyHandThree() {
		
		Hand hand = new Hand();
		hand.addCard(new Card("C","K"));
		hand.addCard(new Card("D","2"));
		hand.addCard(new Card("H","K"));
		hand.addCard(new Card("S","6"));
		hand.addCard(new Card("C","K"));

		assertEquals("This Rank ", 3, hand.rankHand());
	}
	
	@Test
	public void rankMyHandTwoPairs() {
		
		Hand hand = new Hand();
		hand.addCard(new Card("C","T"));
		hand.addCard(new Card("D","J"));
		hand.addCard(new Card("H","T"));
		hand.addCard(new Card("S","J"));
		hand.addCard(new Card("C","4"));
	
		assertEquals("This Rank ", 2, hand.rankHand());
	}
	
	@Test
	public void rankMyHandPair() {
		
		Hand hand = new Hand();
		hand.addCard(new Card("C","Q"));
		hand.addCard(new Card("D","2"));
		hand.addCard(new Card("H","3"));
		hand.addCard(new Card("S","4"));
		hand.addCard(new Card("C","Q"));
	
		assertEquals("This Rank ", 1, hand.rankHand());
	}
	
	@Test
	public void rankMyHandFlush() {
		
		Hand hand = new Hand();
		hand.addCard(new Card("C","Q"));
		hand.addCard(new Card("C","2"));
		hand.addCard(new Card("C","3"));
		hand.addCard(new Card("C","4"));
		hand.addCard(new Card("C","K"));
		
		assertEquals("This Rank ", 5, hand.rankHand());
	}
	
	@Test
	public void rankMyHandStraight() {
		
		Hand hand = new Hand();
		hand.addCard(new Card("H","2"));
		hand.addCard(new Card("C","4"));
		hand.addCard(new Card("C","3"));
		hand.addCard(new Card("D","5"));
		hand.addCard(new Card("C","6"));
	
		assertEquals("This Rank ", 4, hand.rankHand());
	}
	
	@Test
	public void rankMyHandStraightWheel() {
		
		Hand hand = new Hand();
		hand.addCard(new Card("H","A"));
		hand.addCard(new Card("C","2"));
		hand.addCard(new Card("C","3"));
		hand.addCard(new Card("D","5"));
		hand.addCard(new Card("C","4"));
	
		assertEquals("This Rank ", 4, hand.rankHand());
	}
	
	@Test
	public void rankMyHandStraightFlush() {
		
		Hand hand = new Hand();
		hand.addCard(new Card("D","A"));
		hand.addCard(new Card("D","2"));
		hand.addCard(new Card("D","3"));
		hand.addCard(new Card("D","5"));
		hand.addCard(new Card("D","4"));
	
		assertEquals("This Rank ", 8, hand.rankHand());
	}
	
	@Test
	public void getNumberOfCards() {
		
		Hand hand = new Hand();
		hand.addCard(new Card("D","A"));
		hand.addCard(new Card("D","2"));
		hand.addCard(new Card("D","3"));
		hand.addCard(new Card("D","5"));
		hand.addCard(new Card("D","4"));
	
		assertEquals("Number of cards ", 5, hand.amountCards());
	}
}
