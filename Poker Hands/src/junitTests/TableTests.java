package junitTests;

import static org.junit.Assert.*;

import org.junit.Test;

import pokerHands.Hand;
import pokerHands.LogicTable;

public class TableTests {

	@Test
	public void noCard2Times() {
		
		LogicTable table = new LogicTable();
		Hand hand = new Hand();
		String input = "C2";
		
		assertEquals("add card ", "card added", table.addCardToHand(hand, input));
		assertEquals("same card again ", "card already ingame", table.addCardToHand(hand, input));
	}
	
	@Test
	public void checkCorrectInput() {
		
		LogicTable table = new LogicTable();
		Hand hand = new Hand();
		String input1 = "CX";
		String input2 = "Test";
		
		assertEquals("add wrong card ", "wrong input", table.addCardToHand(hand, input1));
		assertEquals("add wrong card ", "wrong input", table.addCardToHand(hand, input2));
	}
	
	@Test
	public void add2HandsAndCompare() {
		
		LogicTable table = new LogicTable();
		Hand firstHand = new Hand();
		table.addCardToHand(firstHand, "C7");
		table.addCardToHand(firstHand, "D7");
		table.addCardToHand(firstHand, "H7");
		table.addCardToHand(firstHand, "S7");
		table.addCardToHand(firstHand, "C6");
		
		Hand secondHand = new Hand();
		table.addCardToHand(secondHand, "CQ");
		table.addCardToHand(secondHand, "DJ");
		table.addCardToHand(secondHand, "HT");
		table.addCardToHand(secondHand, "S8");
		table.addCardToHand(secondHand, "C9");
			
		assertEquals("get winner ", "First hand wins", table.getWinner(firstHand, secondHand));
	}
	
	@Test
	public void testForDraw() {
		
		LogicTable table = new LogicTable();
		Hand firstHand = new Hand();
		table.addCardToHand(firstHand, "H4");
		table.addCardToHand(firstHand, "D6");
		table.addCardToHand(firstHand, "H9");
		table.addCardToHand(firstHand, "ST");
		table.addCardToHand(firstHand, "CQ");
		
		Hand secondHand = new Hand();
		table.addCardToHand(secondHand, "C6");
		table.addCardToHand(secondHand, "D4");
		table.addCardToHand(secondHand, "HT");
		table.addCardToHand(secondHand, "SQ");
		table.addCardToHand(secondHand, "C9");
			
		assertEquals("get winner ", "draw", table.getWinner(firstHand, secondHand));
	}
}
