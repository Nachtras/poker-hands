package pokerHands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class LogicTable {
	private static final String[] suits = {"C","D","H","S"};
	private static final String[] values = {"2","3","4","5","6","7","8","9","T","J","Q","K","A"};
	private List<String> cardsIngame = new ArrayList<String>();
	private List<Hand> hands = new ArrayList<Hand>();
	
	public String addCardToHand(Hand hand, String input) {		
		String cardAdded = "";
		if (Arrays.asList(suits).contains(input.substring(0, 1)) && Arrays.asList(values).contains(input.substring(1))){	
			if (cardsIngame.contains(input)){
				cardAdded = "card already ingame";
			}
			else {
				hand.addCard(new Card((input.substring(0, 1)),(input.substring(1))));
				cardsIngame.add(input);
				cardAdded = "card added";
			}
		}
		else {
			cardAdded = "wrong input";
		}
		return cardAdded;
	}
	
	public String compareHands() {	
		Scanner scanner = new Scanner(System.in);
		for (int i = 0; i<2; i++){
			hands.add(new Hand());
			while (hands.get(i).amountCards() < 5){				
				System.out.println("Enter card for Player "+(i+1));
				String input = scanner.nextLine();
				System.out.println(addCardToHand(hands.get(i), input));
			}
		}
		return getWinner(hands.get(0), hands.get(1));
	}

	public String getWinner(Hand firstHand, Hand secondHand) {
		int firstHandRank = firstHand.rankHand();
		int secondHandRank = secondHand.rankHand();
		if (firstHandRank > secondHandRank)
			return "First hand wins";
		if (firstHandRank < secondHandRank)
			return "Second hand wins";
		else
			return firstHand.compareTo(secondHand);
	}	
}
