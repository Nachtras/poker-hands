package pokerHands;

import java.util.Arrays;

public class Card implements Comparable<Card>{
	private static final String[] values = {"2","3","4","5","6","7","8","9","T","J","Q","K","A"};
	
	private String suit;
	private String value;
	
	public Card (String suit, String value){
		this.suit = suit;
		this.value = value;		
	}
	
	public String getSuit(){
		return suit;
	}
	public String getValue(){
		return value;
	}

	public int getValueForRank() {
		return Arrays.asList(values).indexOf(value);
	}

	@Override
	public int compareTo(Card c) {
		return this.getValueForRank() > c.getValueForRank() ? 1 : (this.getValueForRank() < c.getValueForRank() ? -1 : 0);
	}
}
