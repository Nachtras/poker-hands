package pokerHands;

import java.util.Scanner;

public class PokerHands {

	public static void main(String[] args) {
		playRounds();
	}

	public static int playRounds() {
		int rounds = 0;
		boolean cont = true;
		Scanner scanner = new Scanner(System.in);
		
		while (cont){
			rounds++;
			LogicTable table = new LogicTable();
			System.out.println(table.compareHands());
			System.out.println("play again? (y = yes)");
			String input = scanner.nextLine();
			if (input.equals("y"))
				cont = true;
			else
				cont = false;
				
		}
		scanner.close();
		return rounds;
	}
}
