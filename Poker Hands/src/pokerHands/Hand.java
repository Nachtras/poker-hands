package pokerHands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Hand{
	private int[] highCard = new int[5];
	private List<Card> cards = new ArrayList<Card>();

	public Hand() {
		
	}
	
	public void addCard(Card card) {
		cards.add(card);
	}
	
	// rank a hand and look for the high cards
	public int rankHand() {	
		boolean flush = true;
		boolean straight = false;
		int[] ranks = new int[13];	
		cards.sort(Collections.reverseOrder());
		
		for (int i = 0; i<5; i++){
			ranks[cards.get(i).getValueForRank()]++;
		}
		for (int i = 0; i<4; i++){
			if (!cards.get(i).getSuit().equals(cards.get(i+1).getSuit())){
				flush = false;
			}
		}
		
		List<Integer> sortedRanks = Arrays.stream(ranks).boxed().collect(Collectors.toList()); 
		sortedRanks.sort(Collections.reverseOrder());
		
		// "four of a kind"
		if (sortedRanks.get(0) == 4){
			highCard[0] = cards.get(2).getValueForRank();
			return 7;
		}
		// "full house" or "three of a kind"
		if (sortedRanks.get(0) == 3){
			highCard[0] = cards.get(2).getValueForRank();
			if (sortedRanks.get(1) == 2){
				return 6;
			}
			else {
				return 3;
			}
		}
		// "two pairs" or "pair"
		if (sortedRanks.get(0) == 2){
			int x = 0;
			for (int i=12; i>=0; i--){
				if (ranks[i]==2){
					highCard[x] = i;
					x++;
				}
			}
			for (int i=12; i>=0; i--){
				if (ranks[i]==1){
					highCard[x] = i;
					x++;
				}
			}
			if (sortedRanks.get(1) == 2){
				return 2;
			}
			else {
				return 1;
			}
		}
		// check for a straight wheel
		if (cards.get(0).getValueForRank()==12 && cards.get(1).getValueForRank()==3){
			straight = true;
			highCard[0] = 5;
		}
		// check for normal straight
		if ((cards.get(0).getValueForRank()-cards.get(4).getValueForRank()) == 4){
			straight = true;
			highCard[0] = cards.get(0).getValueForRank();		
		}
		// "straight flush"
		if (flush && straight){
			return 8;
		}
		// "straight"
		if (straight){
			return 4;		
		}	
		
		for (int i = 0; i<5; i++){
			highCard[i] = cards.get(i).getValueForRank();		
		}
		// "flush"
		if (flush){
			return 5;		
		}
		// "high card"
		return 0;	
	}
	
	public int getHighCard(int i){
		return highCard[i];
	}

	public String compareTo(Hand h) {
		for (int i = 0; i<5; i++){
			if (this.getHighCard(i) > h.getHighCard(i))
				return "First hand wins";
			if (this.getHighCard(i) < h.getHighCard(i))
				return "Second hand wins";
		}
		return "draw";
	}

	public int amountCards() {
		return cards.size();
	}
}
